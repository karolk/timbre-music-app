const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');


const isProduction = process.env.NODE_ENV === 'production' // true or false

const cssDev = ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'];

const cssProd = ExtractTextPlugin.extract({
     fallback: 'style-loader',
     use: ['css-loader', 'postcss-loader', 'sass-loader'],
     publicPath: '/dist'
});

const cssConfig = isProduction ? cssProd : cssDev;


module.exports = {
  entry: './src/js/main.js',
  output: {
    filename: '[name].[hash].js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader'
      },
      {
        test: /\.scss$/,
        use: cssConfig
      },
      {
        test: /\.(png|jpg|gif|mp4|webm)$/,
        use: [
          'srcset-loader',
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'img/',
              publicPath: ''
            }
          }
        ]
      },
      {
        test: /\.html$/,
        loader: 'html-loader',
        options: {
          attrs: ['img:src', 'img:srcset', 'source:srcset']
        }
      },
      {
           test: /\.(woff2?|svg)$/,
           use: [
                {
                     loader: 'url-loader',
                     options: {
                          limit: 10000
                     }
                }
           ]
      },
      {
        test: /\.(ttf|eot|woff2?)$/,
        use: [
          {
            loader: 'file-loader'
          }
        ]
      },
    ]
  },
  devServer: {
     port: 6080
  },
  plugins: [

    new CopyWebpackPlugin([
       {
           context: "./src/img/icons/",
           from: "sprite-icons.svg",
           to: "./img"
       }
    ]),

     new HtmlWebpackPlugin({
          minify: {
            collapseWhitespace: true
          },
          hash: true,
          template: './src/index.html',
          title: 'Caching'
     }),
     
     new webpack.optimize.CommonsChunkPlugin({
          name: 'manifest',
          minChunks: Infinity
     }),

    new ExtractTextPlugin({
      filename: '[name].[hash].css',
      disable: !isProduction,
      allChunks: true
    })
  ]
};
