export const Data_Model = (function() {

     const base_url = 'https://api.deezer.com/chart/0/';

     const search_url = 'https://api.deezer.com/search/track?q=';

     const proxy_url = 'https://cors-anywhere.herokuapp.com/';

     let songs = [];
     let albums = [];

     class Player {
          constructor(music) {
               this.music = music;
          }

          skip_song = 1;

          find_song(id) {
               const song = this.music.find(song => song.id === id);

               return {
                    link: song.preview ? song.preview : 'https://e-cdns-preview-5.dzcdn.net/stream/5b16b9c83630367458b8499689f8c23b-4.mp3',
                    cover_small: song.album.cover_small,
                    cover_medium: song.album.cover_medium,
                    artist: song.artist.name,
                    title: song.title
               }
          }

          find_song_index(id) {
              return this.music.findIndex(song => song.id === id);
          }

          find_next_song(id) {
              let song_index = this.find_song_index(id);
              song_index += this.skip_song;
              const check_index = song_index < this.music.length ? song_index : 0;
              return this.music[check_index].id;
          }

          find_prev_song(id) {
              let song_index = this.find_song_index(id);
              song_index -= this.skip_song;
              const check_index = song_index < 0 ? (this.music.length - 1) : song_index;
              return this.music[check_index].id;
          }

          find_first_song() {
              return this.music[0].id;
          }
     };

     let music_player = new Player(songs);

     let header = new Headers({
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
          'Access-Control-Request-Method': 'GET',
          'Access-Control-Allow-Origin': 'http://localhost:6080/',
          'Access-Control-Allow-Credentials': 'true',
          'mode': 'cors'
      });

     const status = (response) => {
          if(response.status >= 200 && response.status < 400) {
               return Promise.resolve(response);
          } else {
               return Promise.reject({
                    status: response.status,
                    statusText: response.statusText
               });
          }
     };

     const fetch_data = (url) => {
          return fetch(proxy_url + url, header)
               .then(status)
               .then(response => response.json())
               .catch(error => alert(Error(`Request filed HTTP status is ${error.status} ${error.statusText}`)));
     };

     return {

          fetch_songs(tab_name = 'tracks') {
               return fetch_data(base_url + tab_name).then(data => {
                    songs = [];
                    songs.push(...data.data)
                    music_player = new Player(songs);
               });
          },

          fetch_albums(tab_name) {
               return fetch_data(base_url + tab_name).then(data => {
                    albums = [];
                    albums.push(...data.data)
               });
          },

          fetch_tracklist(url) {
              return fetch_data(url)
                .then(data => {
                    songs = [];
                    songs.push(...data.data)
                    music_player = new Player(songs);
                });
          },

          fetch_search_tracks(input) {
              return fetch_data(search_url + input)
                .then(data => {
                    if(data.data.length > 0) {
                        songs = [];
                        songs.push(...data.data)
                        music_player = new Player(songs);
                    }
                });
          },

          get_selected_song_link(id) {
               return music_player.find_song(id);
          },

          get_next_song(id) {
              return music_player.find_next_song(id);
          },

          get_prev_song(id) {
              return music_player.find_prev_song(id);
          },

          get_songs() {
               return songs;
          },

          get_first_song_id() {
              return music_player.find_first_song();
          },

          get_albums() {
              return albums;
          },

          get_tracklist(id) {
              return albums.find(album => album.id === id).tracklist;
          }
     }

})();