import { Data_Model } from '../data';
import { Music_UI } from './music_ui';

export const Music_Controller = (function(Data, Music_View) {

     const DOM = Music_View.get_DOM();

     const get_songs = () => Data.get_songs();

     const crtl_tracks = async() => {
          // Add loding animation
          const loading_aniamtion = Music_View.loading_aniamtion();
          // Get songs
          const discover = await Data.fetch_songs();
          const songs = await get_songs();
          // Show songs to the UI
          const show_songs = await Music_View.show_songs({songs: songs});
     };

     const crtl_albums = async(tab_name) => {
          // Add loding animation
          const loading_aniamtion = Music_View.loading_aniamtion({type: 'albums', tab: tab_name});
          // Get songs
          const music_info = await Data.fetch_albums(tab_name)
          const albums = await Data.get_albums();
          // Show songs to the UI
          const show_albums = await Music_View.show_albums(albums, tab_name);
     };

     const crtl_nav_links = async function(e) {
          e.preventDefault();
          const tab_name =  this.getAttribute('href').replace(/\#/g, '');
          // Show section
          const active_setion = Music_View.active_section(tab_name);
          tab_name === 'tracks' ? crtl_tracks() : crtl_albums(tab_name);
     };

     const search = async function(e) {
          e.preventDefault();
          // Get input value
          const input = Music_View.get_search_form_input();
          const tab_name = this.classList[1].replace(/\js--/g, '');
          // Show section
          const active_setion = Music_View.active_section(tab_name);
          const loading_aniamtion = Music_View.loading_aniamtion({tab: tab_name});
          // Find songs
          const find_songs = await Data.fetch_search_tracks(input);
          const songs = await get_songs();
          // Show songs
          const show_songs = await Music_View.show_songs({songs: songs, type: tab_name});
          // Reset form
          const reset = this.reset();
     };

     const set_up_event_listeners = () => {
          const links = Array.from(document.querySelectorAll(DOM.nav_link));
          links.forEach(link => link.addEventListener('click', crtl_nav_links));
          // Search form
          document.querySelector(DOM.search_form).addEventListener('submit', search);
     };

     return {
          init() {
               crtl_tracks();
               set_up_event_listeners();
          }
     }

})(Data_Model, Music_UI);