export const Music_UI = (function() {

     const DOM_string = {
          section: '.js--section',
          songs_box: '.js--songs',
          albums_box: '.js--albums',
          albums_box_artists: '.js--albums-artists',
          albums_box_playlists: '.js--albums-playlists',
          album_id: '.js--album-id',
          nav_link: '.js--nav-link',
          search_form: '.js--search-form'
     };

     const DOM_class = {
         section_active_class: 'section--active'
     };

     return {
          show_songs({songs, type = ''} = {}) {
               const html = songs.map(song => {
                    return `
                    <div class="song">
                         <a href="#${song.id}" class="song__link js--song-id">
                              <img src="${song.album.cover_medium}" alt="Cover song" class="song__cover">
                         </a>
                         <h3 class="song__title">${song.title}</h3>
                         <p class="song__artist">${song.artist.name}</p>
                    </div>
                    `;
               }).join('');
               document.querySelector(`${DOM_string.songs_box}${type !== '' ? `-${type}`: ''}`).innerHTML = html;
          },

          show_albums(albums, tab) {
               const html = albums.map(album => {
                    return `
                    <div class="album">
                         <a href="#${album.id}" class="album__link js--album-id">
                              <img src="${album.picture_medium}" alt="Cover album" class="album__cover">
                         </a>
                         <h3 class="album__title">${album.title ? album.title : album.name}</h3>
                    </div>
                    `;
               }).join('');

               document.querySelector(`${DOM_string.albums_box}-${tab}`).innerHTML = html;
          },

          active_section(tab) {
                Array.from(document.querySelectorAll(DOM_string.section))
                    .forEach(link => link.classList.remove(DOM_class.section_active_class));
                document.querySelector(`${DOM_string.section}-${tab}`).classList.add(DOM_class.section_active_class);
          },

          loading_aniamtion({ type = 'songs', tab = '' } = {}) {
               document.querySelector(`.js--${type}${tab !== '' ? `-${tab}` : ''}`).innerHTML =
                `<div class="spinner">
                    <div class="spinner__loader"></div>
                    <p class="spinner__text">Loading...</p>
                 </div>`;
          },

          get_search_form_input() {
              return document.querySelector(`${DOM_string.search_form} [name=text]`).value.trim().replace(/\s/g, '%20');
          },

          get_DOM() {
              return DOM_string
          }

     }

})();