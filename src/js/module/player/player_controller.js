import { Data_Model } from '../data';
import { Player_UI } from './player_ui';

export const Player_Controller = (function(Data, Player_View) {

     const DOM = Player_View.get_DOM();
     const audio = document.querySelector(DOM.audio);

     const crtl_play = () => {
          // Play or pause a song
          const method = audio.paused ? 'play' : 'pause';
          audio[method]();
          // Change buttons icons
          const change_icons = Player_View.change_icon_player(method);
     };

     const update_song_time = () => {
          const duration = Player_View.convert_time(audio.duration);
          const current_time = Player_View.convert_time(audio.currentTime);
          const time = Player_View.change_song_time(duration, current_time);
     };

     const handle_progress = () => {
          const progress = (audio.currentTime / audio.duration) * 100;
          const progress_bar = Player_View.change_progress_bar(progress);
          const update_time = update_song_time();
     };

     const progress_selected = function(e) {
          const progress_field_width = Player_View.get_progress_bar_width();
          const selected_progress = (e.offsetX / progress_field_width);
          audio.currentTime = selected_progress * audio.duration;
          const progress_bar = Player_View.change_progress_bar(selected_progress);
     };

     const set_song = (id) => {
          // Find song link in data
          const song_info = Data.get_selected_song_link(id);
          // Set song link
          const set_song_link = Player_View.add_song_link(song_info.link, id);
          // Play clicked song
          const play_song = crtl_play();
          // Change cover, artist, title
          const change_song_info = Player_View.add_player_song_info(song_info);
     };

     const selected_song = (e) => {
          const link = e.target.parentNode;
          if(!link.matches(DOM.song_id)) return;
          let song_id = link.getAttribute('href').replace(/\#/g, '');
          song_id = parseInt(song_id);
          // Set song
          const set_song_info = set_song(song_id);
          // Show music player
          const active_player = Player_View.active_player_small();
     };

     const selected_album = async (e) => {
          const link = e.target.parentNode;
          if(!link.matches(DOM.album_id)) return;
          let album_id = link.getAttribute('href').replace(/\#/g, '');
          album_id = parseInt(album_id);
          // Find tracklist url
          const tracklist_url = await Data.get_tracklist(album_id);
          // Fetch album songs
          const fetch_album_songs = await Data.fetch_tracklist(tracklist_url);
          // Get first song id
          const song_id = await Data.get_first_song_id();
          // Set song
          const set_song_info = await set_song(song_id);
          // Show music player
          const active_player = Player_View.active_player_small();

     };

     const get_current_play_song = () => parseInt(Player_View.get_song_id());
     
     const music_player = () => {
          // Get current song id
          const current_song_id = get_current_play_song();
          // Get next song id
          const next_song_id = Data.get_next_song(current_song_id);
          // Set song
          const set_song_info = set_song(next_song_id);
     };

     const play_controllers = () => crtl_play();

     const play_prev = () => {
          // Get current song id
          const current_song_id = get_current_play_song();
          // Play prev song
          const prev_song = Data.get_prev_song(current_song_id);
          // Set song
          const set_song_info = set_song(prev_song);
     };

     const play_next = () => {
          // Get current song id
          const current_song_id = get_current_play_song();
          // Get next song id
          const next_song_id = Data.get_next_song(current_song_id);
          // Set song
          const set_song_info = set_song(next_song_id);
     };

     const volume_range = function() {
          let volume = this.value;
          audio.volume = volume;
          volume *= 100;
          const progress_range = Player_View.change_volume_progress(volume);
     };

     const open_music_player = () => Player_View.active_player_main();

     const set_up_event_listeners = () => {
          document.querySelector(DOM.songs_box).addEventListener('click', selected_song);
          document.querySelector(DOM.songs_search_box).addEventListener('click', selected_song);
          // End of the song
          audio.addEventListener('ended', music_player);
          // time update audio
          audio.addEventListener('timeupdate', handle_progress);
          // Btns controller prev, next
          document.querySelector(DOM.btn_prev).addEventListener('click', play_prev);
          document.querySelector(DOM.btn_next).addEventListener('click', play_next);
          // Progress bar
          document.querySelector(DOM.progress_bar_field).addEventListener('click', progress_selected);
          // Btns play
          const btn_plays = Array.from(document.querySelectorAll(DOM.btn_play));
          btn_plays.forEach(btn => btn.addEventListener('click', play_controllers));
          // Btn view
          const btn_views = Array.from(document.querySelectorAll(DOM.btn_view))
          btn_views.forEach(btn => btn.addEventListener('click', open_music_player));
          // Volume
          document.querySelector(DOM.volume_range).addEventListener('mousemove', volume_range);
          // Play album songs
          const albums = Array.from(document.querySelectorAll(DOM.albums_box))
          albums.forEach(album => album.addEventListener('click', selected_album));
     };

     return {
          event_listeners() {
               set_up_event_listeners();
          }
     }

})(Data_Model, Player_UI);