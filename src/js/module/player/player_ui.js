export const Player_UI = (function() {

     const DOM_string = {
          songs_box: '.js--songs',
          songs_search_box: '.js--songs-search-form',
          albums_box: '.js--albums',
          albums_box_artists: '.js--albums-artists',
          albums_box_playlists: '.js--albums-playlists',
          song_id: '.js--song-id',
          album_id: '.js--album-id',
          audio: '.js--audio',
          btn_play: '.js--btn-play',
          btn_view: '.js--btn-view',
          btn_prev: '.js--btn-prev',
          btn_next: '.js--btn-next',
          icon: '.js--icon',
          icon_controller: '.js--icon-controller',
          player_small: '.js--player-small',
          player_music: '.js--player-music',
          player_small_cover: '.js--player-small-cover',
          player_music_cover: '.js--player-music-cover',
          player_title: '.js--player-title',
          player_artist: '.js--player-artist',
          volume_range: '.js--volume-range',
          volume_range_progress: '.js--volume-progress',
          progress_bar_field: '.js--progress-field',
          progress_bar: '.js--progress-current-field',
          audio_current_time: '.js--song-current-time',
          audio_duration: '.js--song-duration'
     };

     const DOM_class = {
          icon_active_class: 'icon--active',
          player_small_active_class: 'player-music-small--active',
          player_music_active_class: 'player-music--active'
     };

     const convert_numbers = (num) => (num / 100).toFixed(2).replace(/\./g, ':');

     return {

          add_song_link(link, song_id) {
               document.querySelector(DOM_string.audio).src = link;
               document.querySelector(DOM_string.audio).dataset.id = song_id;
          },          

          add_text_info(element, song) {
               Array.from(document.querySelectorAll(element)).forEach(el => el.textContent = song.length > 10 ? `${song.substr(0,24)}...` : song);
          },

          add_player_song_info(song) {
               document.querySelector(DOM_string.player_small_cover).src = song.cover_small;
               document.querySelector(DOM_string.player_music_cover).src = song.cover_medium;
               this.add_text_info(DOM_string.player_artist, song.artist);
               this.add_text_info(DOM_string.player_title, song.title);
          },

          active_player_small() {
               document.querySelector(DOM_string.player_small).classList.add(DOM_class.player_small_active_class);
          },

          active_player_main() {
               document.querySelector(DOM_string.player_small).classList.toggle(DOM_class.player_small_active_class);
               document.querySelector(DOM_string.player_music).classList.toggle(DOM_class.player_music_active_class);
          },

          change_icon_player(method) {
               Array.from(document.querySelectorAll(DOM_string.icon_controller))
                    .forEach(icon => icon.classList.add(DOM_class.icon_active_class));
               Array.from(document.querySelectorAll(`${DOM_string.icon}-${method}`))
                    .forEach(icon => icon.classList.remove(DOM_class.icon_active_class));
          },

          change_volume_progress(progress) {
               document.querySelector(DOM_string.volume_range_progress).style.width = `${progress}%`;
          },

          change_progress_bar(progress) {
               document.querySelector(DOM_string.progress_bar).style.flexBasis = `${progress}%`;
          },

          change_song_time(duration, current_time) {
               document.querySelector(DOM_string.audio_duration).textContent = duration;
               document.querySelector(DOM_string.audio_current_time).textContent = current_time;
          },

          convert_time(time) {
               return !time ? '0:00' : convert_numbers(time);
          },

          get_song_id() {
               return document.querySelector(DOM_string.audio).dataset.id;
          },

          get_progress_bar_width() {
               return document.querySelector(DOM_string.progress_bar_field).offsetWidth;
          },

          get_DOM() {
               return DOM_string;
          }
     }

})();