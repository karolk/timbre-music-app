import { Music_Controller } from './module/music/music_controller';
import { Player_Controller } from './module/player/player_controller';

Music_Controller.init();
Player_Controller.event_listeners();