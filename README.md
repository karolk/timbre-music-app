# Timbre Music App

### Live [Timbre Music](https://timbre-music-app-js.netlify.com/)

![alt text](https://firebasestorage.googleapis.com/v0/b/fir-web-app-40bca.appspot.com/o/photos%2Ftimbre-ios%20album.png?alt=media&token=8753eef3-2e29-4e98-aca4-fe94c612dddb)
![alt text](https://firebasestorage.googleapis.com/v0/b/fir-web-app-40bca.appspot.com/o/photos%2Ftimbre%20iso%20track.png?alt=media&token=e9edcc23-61cf-41c4-93b6-bd1162f1b3b6)

### Description

With "Timbre", it’s easy to find the right music for every moment – on your phone.

There are millions of tracks so whether you’re working out, partying or relaxing, the right music is always at your fingertips. Choose what you want to listen to.

You can browse through the music playlists, albums, artists, top songs, artists.

Design is inspired by Apple Music.

## Project structure and implementation of the project have been implemented and designed by Karol Kozer.

### The project includes:

ES6, Sass, Webpack, Babel, Deezer API

### Deezer API

Unlimited Access, without stress, without identification. Deezer Simple API provides a nice set of services to build up web applications allowing the discovery of Deezer's music catalogue.

[Deezer](https://developers.deezer.com/api)

### Run project

Require [Node](https://nodejs.org/en/)

Install the dependencies:

```
npm install
```

Run dev server:

```
npm run dev
```

Localhost:
`http://localhost:6080/`
